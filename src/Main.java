import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        // Установка длины массива
        System.out.print("Введите длину массива: ");
        int dlina = in.nextInt();
        // Проверка длины массива
        int[] myList;
        if (dlina > 0) {
            boolean[] chetnoe;
            myList = new int[dlina];
            chetnoe = new boolean[dlina];
            // Занесение значений массива
            for (int i = 0; i < dlina; i++) {
                System.out.print("Введите значение массива: ");
                myList[i] = in.nextInt();
            }
            // Вывести на экран все элементы массива
            for (int i = 0; i < myList.length; i++) {
                System.out.print(myList[i] + " ");
            }
            // Сумма элементов массива
            double total = 0;
            for (int i = 0; i < myList.length; i++) {
                total += myList[i];
            }
            System.out.println("\nСумма чисел массива: " + total);
            // Нахождение среди элементов массива наибольшего
            int max = myList[0];
            for (int i = 1; i < myList.length; i++) {
                if (myList[i] > max) max = myList[i];
            }
            System.out.println("Наибольший элемент: " + max);
            // Нахождение среднего арифмитического массива
            double sred = total / myList.length;
            System.out.println("Среднее арифмитическое: " + sred);
            // Проверка на четность
            System.out.print("Является ли число четным? ");
            for (int i = 0; i < myList.length; i++) {
                if (myList[i] % 2 == 0) {
                    chetnoe[i] = true;
                } else {
                    chetnoe[i] = false;
                }
                System.out.print(chetnoe[i] + " ");
            }
            //Конвертация массива в строку
            String str = "";
            for (int i = 0; i < myList.length; i++) {
                str = (str + (myList[i]));
            }
            System.out.println("\n" + str);
            //Конвертация строки в массив
            char[] chArray = str.toCharArray();
            for (int i = 0; i < chArray.length; i++) {
                System.out.print(chArray[i]);
            }
            System.out.print("\n");
            //Конвертация в массив INT
            int[] arrInt =new int[chArray.length];
            for (int i = 0; i < chArray.length; i++) {
                arrInt[i] =  (chArray[i] - '0');
                System.out.print(arrInt[i]);
            }
        } else {
            System.out.println("Длина массива не может быть меньше 1");
        }
    }
}